(function(){
  var m=angular.module('westin', ['rest']);
  
  m.controller('dailyCtrl', ['$scope', '$log', '$sce', 'daily', 'viewer', function(scope, log, sce, daily, viewer){
    this.pdfUrl=null;
    this.date=new Date();
    this.info=[];

    var self=this;
    this.setDate=function(_date){
      _date=_date || this.date;
      if(_date instanceof Date){
	log.info('change to: '+_date);
	self.date=_date;
	daily(_date, function(data){
	  self.info=data;
	  log.info(data);	  

	});
      }else{
	
	log.error("not a date");
      }
    };

    this.setDate(this.date);
    this.showfile=function(dirname, fn){
      var url=viewer(this.date, dirname, fn);
      this.pdfUrl=sce.trustAsResourceUrl(url);
      log.info("viewer url: "+this.pdfUrl);
    };

  }]);
  
  m.directive('date', function(){
    return{
      restrict: "E",
      template: "<input ng-model='this.date' type='date'></input>"
    };
  });

  
})();
