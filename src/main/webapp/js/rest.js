(function(){
  var m=angular.module('rest',[]);
  m.factory('daily', ['$http', '$log', 'dateFilter', function(http, log, dateFilter){
    var req=function(date, update){
      var success=function(data, status, headers, cfg){
	update(data);
      };
      var error=function(){
      };
      
      var httpCfg={method: 'GET', url: "res/daily/"+dateFilter(date, 'yyyyMMdd')};

      http(httpCfg).success(success).error(error);
      
    };
    return req;

    
  }]);
  
  m.factory('viewer', ['$log', 'dateFilter', function(log, dateFilter){
      var googlePDFViewer="https://docs.google.com/gview?embedded=true&url=";
      var s3head="https://s3.amazonaws.com/westin.scanner/dropbox";

      return function(date, dirname, fn){
	  var uDirname=encodeURIComponent(dirname);
	  var uFn=encodeURIComponent(fn);
      var url=googlePDFViewer
	      +encodeURI(s3head+(dateFilter(date, '/yyyyMM/dd')+"/"+uDirname+"/"+uFn));
      return url;
      
    };
    
  }]);
})();
