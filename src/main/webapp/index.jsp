<!doctype html>
<html lang='en' >
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <%

       String ip=request.getServerName();
       boolean local=ip.equals("localhost");
       if(local){
       %>
    
    <link href="webjars/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <%} else{ %>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    
    
    <%
       }
       %>

    <!--
	<link href="css/my.css" rel="stylesheet">
	http://localhost:8080/westin/res/daily/20160417      
	-->
  </head>
  <body style='background-color:#eeeeee' ng-app='westin' ng-controller='dailyCtrl as daily' >
    <!-- starting content -->
    <div class='container'>

      <nav class='navbar navbar-default' style='background-color: #a2ae4c;'>
	<div class='container-fluid'>
	  <div class='navbar-header'>
	    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
	    </button>
	    <a class='navbar-brand' style='color: white'>Westin</a>
	  </div>
	  
	  <ul class='nav nav-pills' >
	    <li><a style='color: white' ng-show='daily.pdfUrl' ng-click='daily.pdfUrl=null'>return to file list</a></li>
	  </ul>
	</div>
      </nav>

      <div style='height:100%'>
	<div ng-hide='daily.pdfUrl'>
	  <!-- input date -->
	  <div class='well-sm center-block row'>
	    <input class='glyphicon glyphicon-calendar center-block text-center text-nowrap' ng-model='daily.date' type='date'  ng-change='daily.setDate()'></input>
	  </div>

	  <!--data show -->
	  <div class='col-sm-10' col-sm-offset-1' style='margin-bottom: -99999px; padding-bottom:99999px' >
	    
	    <div ng-hide='daily.pdfUrl'>
	      <ul ng-repeat='dir in daily.info'>
		<li class='glyphicon list-group'><button data-toggle='collapse' target="{{'#'+dir.name}}" class='label label-default'>{{dir.name}}</button><span class='badge'>{{dir.files.length}}</span>
		  <ul id="{{dir.name}}"  ng-repeat='fn in dir.files'><li class='list-group-item' ng-click='daily.showfile(dir.name, fn)'> {{fn}}</li></ul>
		</li>
	      </ul>
	    </div>
	    
	  </div>
	</div>

	<style>
	  .remaining {
	  height: -webkit-calc(100vh - 72px);
	  height: -moz-calc(100vh - 72px);
	  height: calc(100vh - 72px);
	  background-color: rgba(90,90,190,0.8);
	  } 
	</style>
	<!-- embedded pdf viewer-->
	<!-- 4:3 aspect ratio -->

	    <div class="embed-responsive embed-responsive-4by3" ng-show='daily.pdfUrl'>

		<!--
	<div style="height:100%" ng-show='daily.pdfUrl'>
	      -->
	  <iframe style='height:100%' src="{{daily.pdfUrl}}"></iframe>
	</div>


      </div>

    </div>
    <!-- end content -->

    <!-- starting the script part -->
    <%
       
       if(local){
       %>
    <script type="text/javascript" src="webjars/angularjs/1.5.5/angular.js"></script>
    <script type="text/javascript" src="webjars/jquery/2.0.0/jquery.js"></script>
    <script type="text/javascript" src="webjars/bootstrap/3.3.6/js/bootstrap.js"></script>
    
    
    <%
       }else{
       %>
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <%
       }
       %>
    <script type="text/javascript" src="./js/rest.js"></script>
    <script type="text/javascript" src="./js/westin.js"></script>
  </body>

</html>
