package biz.emenu.westin.rest;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.persistence.oxm.annotations.XmlAccessMethods;

import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/daily/{date: 201\\d[01]\\d[0-3]\\d}")
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class DailyInfo implements Serializable {

	/**
 * 
 */
	
	
	private static final long serialVersionUID = 1L;

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.NONE)
	public static class Dir {
		@XmlElement
		public String name;
		@XmlElement
		public List<String> files;

		public Dir() {
			this.files = new ArrayList<>();
		}
		
		public Dir(String name, String[] files){
			this();
			this.name=name;
			for(String file: files){
				this.files.add(file);
			}
		}
	}

	

	public void setDay(String date) {
		

	}

	S3Tool st = null;
	String lockSt = "";

	private List<Dir> realList(String date) {
		synchronized (lockSt) {
			if (st == null) {
				this.st = new S3Tool();
			}
		}
		Map<String, List<String>> dirm=new HashMap<>();
		try{
			dirm = this.st.listObject(date, "pdf");
		}catch(Exception e){
			this.st.expired=true;
		}
		List<Dir> result=new ArrayList<>();
		for (String d : dirm.keySet()) {
			Dir dir = new Dir();
			dir.name = d;
			dir.files = dirm.get(d);
			result.add(dir);
		}
		return result;
	}

	
	private List<Dir> debugInfo(String date) {

		Map<String, Dir[]> dirs=new HashMap<>();
		Dir dir20160615=new Dir("Bos",new String[]{
				"LE MOYNE (Runs Tomorrow) [ 6-00AM-11-59PM - ] [BEO-- 32,074].pdf",
				"ST-PAUL (Runs Tomorrow) [ 6-00 AM-11-59 PM - ] [BEO-- 32,067].pdf",
				"TOUR STA (Runs Tomorrow) [ 6-01 AM-11-59 PM Salon 7 - tour Printing room N] [BEO-- 32,121].pdf"
				});
		Dir dir20160726=new Dir("Bos",new String[]{
				"ALCOVE (NOW RUNNING) [6h00AM@11h00PM Foyer 9 escaliers] [Bc# 36,115] .pdf",
				"MCGILL (NOW RUNNING) [8h00AM@5h00PM ] [BEO# 36,051] .pdf",
				"RAMEZAY (NOW RUNNING) [Event Name] [10h00AM@4h00PM ] [BEO# 36,037] .pdf",
				"RAMEZAY-FOYER (Done for today) [1h45PM@2h45PM ] [BEO# 36,035] .pdf",
				"RAMEZAY-FOYER (NOW RUNNING) [10h00AM@10h30PM ] [BEO# 36,135] .pdf",
				"RIOPELLE (Starts in 2hrs 6mins) [5h00PM@6h00PM ] [BEO# 36,117] .pdf",
				"SALON 5 (NOW RUNNING) [6h01AM@11h59PM tour sainthantoine Trainers Lounge@OSP N 20] [BEO# 35,393].pdf",
				"SALON 6 (NOW RUNNING) [6h01AM@11h59PM tour sainthantoine] [BEO# 35,394].pdf",
				"SALON 7 (NOW RUNNING) [6h01AM@11h59PM tour sainthantoine] [BEO# 35,394].pdf",
				"SALON 8 (NOW RUNNING) [6h01AM@11h59PM tour sainthantoine] [BEO# 35,394].pdf",
				"TBA (NOW RUNNING) [6h01AM@11h59PM Bain Office Room 516 OFF N 4] [BEO# 35,395] .pdf",
				"TBA (NOW RUNNING) [6h01AM@11h59PM CONVERTED ROOM Bh0 N] [BEO# 35,391] .pdf",
				"TERRASSE (NOW RUNNING) [6h00AM@11h59PM ] [Bc# 36,113] .pdf","VICTORIA (Starts in 3hrs 6mins) [6h00PM@10h00PM ] [BEO# 36,185] .pdf",
				"VICTORIA (Starts in 3hrs 6mins) [6h00PM@11h59PM ] [BEO# 36,118] .pdf","VIGER (Done for today) [8h45AM@9h45AM ] [BEO# 36,034] .pdf",
				"YOUVILLE (NOW RUNNING) [0h00PM@6h00PM ] [Bc# 36,140] .pdf","[BEO# 35,391] .pdf","[BEO# 36,117] .pdf","w013.pdf","w014.pdf","w020.pdf"
				});
		Dir dir20160702=new Dir("Bos",new String[]{
				"LE MOYNE (NOW RUNNING) [6h00AM@11h59PM] [BEO# 32,074].pdf",
				"ST-PAUL (NOW RUNNING) [6h00AM@11h59PM] [8h00AM@11h00PM] [BEO# 32,067].pdf",
				"TOUR STA (NOW RUNNING) [10h00AM@11h59PM Salon 7 tour Printing room N]  [BEO# 32,074].pdf"
				});
		dirs.put("20160615", new Dir[]{dir20160615});
		dirs.put("20160726", new Dir[]{dir20160726});
		dirs.put("20160702", new Dir[]{dir20160702});
		Dir[] ds=dirs.get(date);
		List<Dir> result=new ArrayList<>();
		if(ds!=null){
			for(Dir dd: ds){
				result.add(dd);
			}
		}
		return result;
	}
	

	private boolean isLocalhost(HttpServletRequest req){
		return "localhost".equals(req.getServerName());
	}

	Logger log=LogManager.getLogger(DailyInfo.class);
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dir> get(@Context HttpServletRequest req, @PathParam("date") String date) {
		List<Dir> dirs=null;
		boolean debug=this.isLocalhost(req);

		if(debug){
			log.info("localhost, debuging");
			dirs=this.debugInfo(date);
		}else{
			log.info("production");
			dirs=this.realList(date);
		}
		System.out.println("get()");
		return dirs;
	}
}