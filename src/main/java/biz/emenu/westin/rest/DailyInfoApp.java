package biz.emenu.westin.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/res")
public class DailyInfoApp extends Application{
	public DailyInfoApp(){
		super();
	}
	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> cz=new HashSet<>();
		cz.add(DailyInfo.class);
		cz.add(S3Tool.class);
		return cz;
	}
	
	
}
