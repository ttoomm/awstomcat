package biz.emenu.westin.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import biz.emenu.westin.rest.DailyInfo.Dir;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@Singleton
public class S3Tool {

	static private AmazonS3 s3;
	final Regions myRegion = Regions.US_EAST_1;
	final public String bkname = "westin.scanner";
	
	public boolean expired=true;

	public S3Tool() {
		this.getInstance();
		this.expired=false;
	}
	
	

	synchronized private void getInstance() {
		if (expired|| s3 == null) {
			AWSCredentials credentials = null;
			try {
				// InstanceProfileCredentialsProvider credentialsProvider = new
				// InstanceProfileCredentialsProvider().get;
				credentials = new InstanceProfileCredentialsProvider()
						.getCredentials();
			} catch (Exception e) {
				System.out
						.println("fail to get credentials: credentialsProvider");
				try {
					credentials = new ProfileCredentialsProvider()
							.getCredentials();
				} catch (Exception ee) {
					throw new AmazonClientException(
							"Cannot load the credentials from the credential profiles file. "
									+ "Please make sure that your credentials file is at the correct "
									+ "location (~/.aws/credentials), and is in valid format.",
							e);
				}
			}
			s3 = new AmazonS3Client(credentials);
			Region region = Region.getRegion(myRegion);
			s3.setRegion(region);
		}

	}

	Map<String, List<String>> listObject(String date, String postFix) {
		/*
		 * List objects in your bucket by prefix - There are many options for
		 * listing the objects in your bucket. Keep in mind that buckets with
		 * many objects might truncate their results when listing their objects,
		 * so be sure to check if the returned object listing is truncated, and
		 * use the AmazonS3.listNextBatchOfObjects(...) operation to retrieve
		 * additional results.
		 */

		String strMonth = date.substring(0, 6);
		String strDay = date.substring(6);
		String prefix = String.format("dropbox/%s/%s/", strMonth, strDay);
		int lenPrefix = prefix.length();
		Map<String, List<String>> dirs = new HashMap<>();

		System.out.println("listing, with prefix: " + prefix);

		// prefix = "dropbox";

		// List<String> entries = new ArrayList<>();
		
		List<ObjectListing> objss = new ArrayList<>();
		ListObjectsRequest req = new ListObjectsRequest().withBucketName(
				this.bkname).withPrefix(prefix);

		ObjectListing objs = s3.listObjects(req);
		objss.add(objs);
		while (objs.isTruncated()) {
			objs = s3.listNextBatchOfObjects(objs);
			objss.add(objs);
		}

		
		for (ObjectListing o : objss) {

			for (S3ObjectSummary summary : o.getObjectSummaries()) {
				String key = summary.getKey();
				if (key == null || key.length() <= lenPrefix)
					continue;
				String filepath = key.substring(lenPrefix);
				String[] inf = filepath.split("/");
				if (inf.length != 2) {
					System.err.println("unqualified key: " + key);
					continue;
				}
				String dir = inf[0];
				String fn = inf[1];
				if(!fn.endsWith(postFix)){
					continue;
				}
				if (dirs.get(dir) == null) {
					List<String> fns = new ArrayList<>();
					dirs.put(dir, fns);
				}
				dirs.get(dir).add(fn);
			}
		}

		System.out.println("listing end");
		return dirs;
	}

}
