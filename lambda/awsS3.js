/*
 * Copyright 2013. Amazon Web Services, Inc. All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
// Load the SDK and UUID

var AWS = require('aws-sdk');
require('./dateTool');
//var uuid = require('node-uuid');

// Create an S3 client
var s3 = new AWS.S3();

// Create a bucket and upload something into it
var bucketName = 'westin.scanner';
var keyName = 'a/b/hellod_world.txt';
var keyBase='dropbox';


var s3update=function(bkt, fkey, version, callback){
   
  //s3.createBucket({Bucket: bucketName}, function() {
    var params = {
      Bucket: bucketName,
      Key: fkey,
      Body: 'Hello World!',
      Metadata:{
	dbx_v:version
      }
    };
    s3.putObject(params, callback);
//  });
  
};

var s3meta=function(bkt, fkey, callback){
  var param={
    Bucket: bkt,
    Key: fkey
  };
  s3.headObject(param, callback);
};



var cb=function(err, data){
  if(err){
    console.error("error: ");
    console.error(err);
    return;
  }
  console.log('data: ');
  console.log(data);
};



var s3dailyInfo=function(bkt, dir, callback){

  var paras={
    Bucket: bkt,
    Prefix: dir
  };
  
  var f= function(objs, ar, callback){
    var e=ar.pop();
    s3.headObject({Bucket: bucketName, Key: e.Key}, function(err, d){
      if(err){
	console.error(err);
	callback(err, null);
	return;
      }
      objs=objs.concat({path: e.Key.substr(keyBase.length, e.Key.length-1), size: d.ContentLength-0, version: d.Metadata?d.Metadata.dbx_v: null});
      if(ar.length>0){
	f(objs, ar, callback);
      }else{
	callback(null, objs);
      }
    });

  };

  var _cb=function(err, ar){
    if(err){
      if(callback){
	callback(err, null);
	return;
      }
    }
    //console.log(ar);
    if((!ar) || (!ar.Contents) ||(ar.Contents.length==0)){
      callback(null, []);
      return;
    }
    //console.log(ar.Contents);
    f([], ar.Contents, callback);
  };
  
  s3.listObjects(paras, _cb);
};




exports.dailyInfo=function(day, callback){
  var dailyDir=day.ymd('ym')+'/'+day.ymd('d');
  var dir=keyBase+'/'+dailyDir;
  console.log("[s3]start retriving information, dir: "+dir);
  s3dailyInfo(bucketName, dir, callback? callback: cb);
};

exports.saveFile=function(path, data, version, callback){
  var param={
    Bucket: bucketName,
    Key: keyBase+path,
    ACL: 'public-read',
    Body: data,
    Metadata:{
	dbx_v:version
    }
  };
  return s3.putObject(param, callback);
};

exports.removeObj=function(path, callback){
  var param={
    Bucket: bucketName,
    Key: keyBase+path
  };
  s3.deleteObject(param, callback);
};

//s3update(bucketName, keyName, "fff", cb);

exports.handler=function(evt, ctx){


//s3meta(bucketName, keyName, cb);

//s3dailyInfo(bucketName, new Date(), cb);
  
//f(new Date(), cb);




  
  //var s=JSON.stringify(evt);
  
//  console.log(s);
  //  ctx.succeed(evt);
  
};


