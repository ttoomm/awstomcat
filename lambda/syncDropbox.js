#!/usr/bin/node

process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
var AWS = require('aws-sdk');
var as3= require('./awsS3.js');
var dbx= require('./dbx.js');



var sync=function(day, callback){

  var cb1=function(ar){
    return function(err, data){
      if(err){
	console.error("fail to get s3 file list, error: ");
	console.error(err);
	return;
      }
      console.log("get files from s3: ");
      console.log(data);
      //from dbx to aws
      ar.forEach(function(e){
	var exist=data.some(function(ee){
	  return (ee.path===e.path) &&
	    (ee.version==e.version) &&
	    (ee.size==e.size);
	});
	if(exist){
	  return;
	}
	console.log("try to sync file from dbx: ");
	console.log(e.path);
	
	dbx.getFileContent(e.path, function(err, buf){
	  console.log("file uploading, info:");
	  console.log(e);
	  if(err){
	    console.error('error when sync from dbx to s3 :');
	    console.error(err);
	    return;
	  }
	  as3.saveFile(e.path, buf, e.version, function(ee, dd){
	    if(ee){
	      console.error("fail to upload file, "+e.path);
	      console.dir(ee);
	      return;
	    }
	    console.log('sync file '+e.path+', return: ');
	    console.log(dd);
	  });
	});
      });
      //remove file from s3 which doesn't exist in dropbox
      data.forEach(function(e){
	var exist=ar.some(function(ee){
	  return ee.path==e.path;
	});
	if(exist) {
	  return;
	}
	as3.removeObj(e.path, function(e, d){
	  if(e){
	    console.error("error when remove file +["+e.path+"] from s3, error: ");
	    console.error(e);
	    return;
	  }
	  console.log("removed file +["+e.path+"] from s3, return message: ");
	  console.log(d);
	});
      });
    };
  };
  
  var cb=function(error, data){
    if(error){
      console.error(error);
      callback && callback(error, null);
      return;
    }
    console.log("get data from dropbox: ");
    console.log(data);
    var f=cb1(data);
    as3.dailyInfo(day, f);
  };
  
  dbx.dailyInfo(day, cb);
};

//var day=new Date((new Date())-1000*24*3600*2);
//sync(day);

syncDays=function(days){
  /*
  if(days<1) {
    console.log("at least one day");
    return;
  }
*/
  var today=new Date();
  for(var i=0; i<Math.abs(days); i++){
    var day=new Date(today-0+i*1000*24*3600);
    console.log("start sync-ing: "+day);
    sync(day);
  } 
};



exports.handler=function(evt, ctx){
  var s=JSON.stringify(evt);
  
  if((!evt)||(!evt.days)||(evt.days>200)||(evt.days<-100)) {
    s="illegal input: \n"+s;
    console.error(s);
    ctx.fail(s);
    return;
  }
  console.log("----------->receive event: ");
  console.log(s);
  console.log("--------------start sync-ing.");
  syncDays(evt.days);
  
};





