#!/bin/bash
#. /home/hh/ws/lambda/src/main/resources/org/killqq/taskmanager/lambda/credentials.ini
#echo $admin_username
#echo $admin_password

aws lambda invoke \
--region us-east-1 \
--function-name sample \
--payload "{\"date\": \"20160810\"}" \
--invocation-type RequestResponse \
    ./response
echo "[response]: "
cat ./response
echo
echo
