#!/bin/bash

zipfile=rest.zip
lambdaFuns="listDaily"
role=arn:aws:iam::484888238703:role/lambda_s3_exec_role

rm -f $zipfile
#cd js
#zip -r   ../$zipfile . -x node_modules/aws-sdk/**\*
zip    ./$zipfile ./listDaily.js
#cd ..
aws s3 cp ./$zipfile s3://taskmanager.westin/$zipfile  --acl bucket-owner-full-control


for lambdaFun in $lambdaFuns; do
    echo "---delete function: "
    cmd="aws lambda delete-function --function-name $lambdaFun"
    echo $cmd
    $cmd
    echo

    echo "--start create-function"
    cmd="aws lambda create-function \
--function-name $lambdaFun \
--code S3Bucket=taskmanager.westin,S3Key=$zipfile \
--role $role \
--handler ${lambdaFun}.handler \
--runtime nodejs4.3 \
--timeout 15 \
--memory-size 512 "
    echo $cmd;
    $cmd;
    echo;
done;







