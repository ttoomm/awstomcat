#!/bin/bash

zipfile=rest.zip
lambdaFuns="syncBos"

action='create-function'
action='update-function-code'


role=arn:aws:iam::484888238703:role/lambda_s3_exec_role

rm -f $zipfile
#cd js
zip -r   ../$zipfile  . -x node_modules/aws-sdk/**\*
#zip    ./$zipfile ./${lambdaFuns}.js

#cd ..


aws s3 cp ./$zipfile s3://taskmanager.westin/$zipfile  --acl bucket-owner-full-control



for lambdaFun in $lambdaFuns; do
    echo "---delete function: "
    cmd="aws lambda delete-function --function-name $lambdaFun"
    echo $cmd
    #$cmd
    echo
    para="--code S3Bucket=taskmanager.westin,S3Key=$zipfile "
    para="--s3-bucket=taskmanager.westin --s3-key=$zipfile "
    tail="--role $role \
--handler ${lambdaFun}.handler \
--runtime nodejs4.3 \
--timeout 15 \
--memory-size 512 "
    tail="--publish"
    echo "--start ${action}"
    cmd="aws lambda ${action} \
--function-name $lambdaFun \
${para} \
${tail}"
    echo $cmd;
    $cmd;
    echo;
done;







