#!/bin/bash

zipfile=rest.zip
lambdaFuns="listDaily"
stage='Test'
bkName='westin.test'
action='create-function'
action='update-function-code'

role=arn:aws:iam::484888238703:role/lambda_s3_exec_role

rm -f $zipfile
cd code
zip -r ../$zipfile ./listDaily.js ./dateTool.js ./syncBos.js 
cd ..


aws s3 cp ./$zipfile s3://$bkName/$zipfile  --acl bucket-owner-full-control

for lambdaFun in $lambdaFuns; do
    if [ $action == 'create-function' ]
    then
	echo "---delete function: "
	cmd="aws lambda delete-function --function-name $lambdaFun$stage"
	echo $cmd
	$cmd
	echo
	para="--code S3Bucket=$bkName,S3Key=$zipfile "
	tail="--role $role \
--handler ${lambdaFun}.handler \
--runtime nodejs4.3 \
--timeout 15 \
--memory-size 512 "
    fi

    if [ $action == 'update-function-code' ]
    then
	para="--s3-bucket=$bkName --s3-key=$zipfile "
	tail="--publish"
	echo "--start ${action}"
    fi
    
    cmd="aws lambda ${action} \
--function-name $lambdaFun$stage \
${para} \
${tail}"
    echo $cmd;
    $cmd;
    echo;
done;







