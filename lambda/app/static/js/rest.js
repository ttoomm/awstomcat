(function(){
    var debug=false;

    var m=angular.module('rest',[]);
    m.factory('daily', ['$http', '$log', 'dateFilter', function(http, log, dateFilter){
	var req=function(date, update){
	    var success=function(data, status, headers, cfg){
		//update(data.dirs);
		update(data);
	    };
	    var error=function(){
	    };
	    
	    var resturl="https://emulsbeow5.execute-api.us-east-1.amazonaws.com/testing/bos/daily/";
	    if(debug)resturl="https://emulsbeow5.execute-api.us-east-1.amazonaws.com/test/bos/daily/";
	    
	    var httpCfg={method: 'GET', url: resturl+dateFilter(date, 'yyyyMMdd')};
	    if(debug) httpCfg={method: 'GET', url: resturl+'20160922'};

	    http(httpCfg).success(success).error(error);
	    
	};
	return req;

	
    }]);
    
    m.factory('viewer', ['$log', 'dateFilter', function(log, dateFilter){
	var googlePDFViewer="https://docs.google.com/gview?embedded=true&url=";
	var urlS3="https://s3.amazonaws.com/westin.scanner/";
	if(debug) urlS3="https://s3.amazonaws.com/westin.test/";
	return function(date, dirname, fn){
	    var url=null;
	    if(fn.startsWith("http")){
		url=fn;
	    }else{
		url=urlS3+fn;
	    }
	    
	    if(url.endsWith('png')){
		return {pdf: null, img: url};
	    }else if(url.endsWith('pdf')){
		url=googlePDFViewer+encodeURIComponent(url);
		console.info(url);
		return {pdf: url, img: null};
	    }else{
		return {pdf:url, img:null};
	    }
	};
	
    }]);
})();
