(function(){
    var debug=false;
    var m=angular.module('westin', ['rest']);
    
    m.controller('dailyCtrl', ['$scope', '$log', '$sce', 'daily', 'viewer', '$http', function(scope, log, sce, daily, viewer, http){
	const LAYOUTLOC="https://s3.amazonaws.com/pub.westin/staticHtml/media/"
	const FLOORS=[{
		fn: "FLOOR 3",
		path: LAYOUTLOC+"f3.png"
	    },{
		fn: "FLOOR 8",
		path: LAYOUTLOC+'f8.png'
	    },{
		fn: "FLOOR 9",
		path: LAYOUTLOC+"f9.png"
	    },{
		fn: "FLOOR 11",
		path: LAYOUTLOC+"f11.png"
	    },{
		fn: 'TOWER',
		path: LAYOUTLOC+"tower.png"
	    }];

	this.floors={};
	this.floors.name="floor";
	this.floors.files=[];
	for(var floor in FLOORS){
	    this.floors.files.push(FLOORS[floor]);
	}


	this.pdfUrl=null;
	this.imgUrl=null;
	this.date=new Date();
	this.info=[];
	this.showFileList=true;
	this.showImg=false;
	this.showIframe=false;
	this.token="";
	this.cookie=null;
	if(debug)this.cookie="a";
	this.message="Please login";

	var sort=function(arr, t){
	    var fccc=function(a,b){
		if(a==null && b==null) return 0;
		if(a!=null && b==null) return 1;
		if(a==null && b!=null) return -1;
		return "continue";
	    };
	    var fcc=function(a, b, p){
		var r=fccc(a,b);
		if(r=="continue"){
		    r= fccc(a[p], b[p])
		    if(r=='continue'){
			if(a[p]>b[p]) return 1;
			if(a[p]==b[p]) return 0;
			if(a[p]<b[p]) return -1;
		    }
		}
		return r;
	    };
	    return arr.sort(function(a, b){
		console.info("sorting by ", t);
		return fcc(a,b,t);
	    });
	};


	var self=this;
	this.setDate=function(_date){
	    if(!this.cookie) return;
	    self.info=[];
	    _date=_date || this.date;
	    if(_date instanceof Date){
		log.info('change to: '+_date);
		self.date=_date;
		var _info={};
		daily(_date, function(data){
		    data=data['body-json'];
		    if(data.status==401){
			self.cookie=null;
			self.message="Expired, Login again";
			return;
		    }
		    data=data['data'];
		    var d=data.map(function(e){return e.Key});
		    var re=/^dropbox\/20\d{4}\/\d{2}\/[^\/]+\/.+$/;
		    d.map(function(e){
			if(!re.test(e)){
			    return null;
			}
			if(!(e.endsWith(".pdf") || e.endsWith('png'))){
			    return null;
			}
			var dd=e.split('/');
			_info[dd[3]]=_info[dd[3]]||[];
			var fn=decodeURIComponent(dd[4]).replace(/\+/g, " ");
			//if this is bo pdf, then extract the begin and end time
			var info=getTimeBeforeEvent(fn, _date);
			fn=info.fn;
			_info[dd[3]].push({fn: fn, path: e, status:info.status, statusValue: info.statusValue, room: info.room, bo: info.bo, starting: info.starting});
			//_info[dd[3]].push({fn: fn, path: e, status:"ongoing", statusValue: info.statusValue});
			return null;
		    });

		    self.info=[];
		    
		    for(var i in _info){
			if(/^\w+$/.test(i)){
			    var dirInfo={name: i, files:_info[i]};
			    if(i.toLowerCase()=='bos'){
				dirInfo.sort=sort;
			    }
			    self.info.push(dirInfo);
			}
		    }

		      
		    var floorExist=false;
		    var floorInfo=null;
		    for(var i in self.info){
			if(self.info[i].name.toLowerCase()=='floor'){
			    floorExist=true;
			    floorInfo=self.info[i];
			    break;
			}
		    }
		    if(!floorExist){
			floorInfo={name: 'floor', files:[]};
			self.info.push(floorInfo);
		    }
		    for(var i in floorInfo.files){
			floorInfo.files[i].status='roomstatus';
		    }
		    floorInfo.files=FLOORS.concat(floorInfo.files);

		    log.info(self.info);	  

		});
	    }else{
		
		log.error("not a date");
	    }
	};

	var getTimeBeforeEvent=function(fn, dDir){
	    //regex for matching Bo pdf filename
	    var info={fn: fn, status: 'unknown', room: null, startAt: null, bo: 0};
	    var regexBos=/.+.pdf$/;


	    if(!regexBos.test(fn)){
		return info;
	    }
	    var regexB=/\[[^\]]+\]/;//to match the square bracket
	    var regexT=/\d+h\d*[AP]M/g; //to match the ddh22AM
	    
	    var t=fn.match(regexB);
	    if(!(t && t.length)){
		return info;
	    }
	    var sa=t[0];
	    
	    var ab=sa.match(regexT);
	    if(!(ab && ab.length)){
		return info;
	    }
	    

	    var strStart=ab[0];//xxh22PM
	    var strEnd=ab[1];
	    //console.info(fn);
	    //console.info(strStart, strEnd, dDir);
	    //get the precise oclock for the xxhxxPM
	    var getclock=function(s){
		if(!s)return null;
		var hm=s.match(/\d+/g);
		if(hm && hm.length){
		    var h=hm[0]||0;
		    h=h-0;
		    var m=hm[1]||0;
		    m=m-0;
		    if(s.toUpperCase().endsWith('PM')){
			h=(h==12)?h:h+12;
		    }
		    var tm=new Date(dDir);
		    tm.setHours(h);
		    tm.setMinutes(m);
		    return tm;
		}
	    }
	    var dStart=getclock(strStart);
	    info.starting=dStart;
	    var dEnd=getclock(strEnd);
	    var now=new Date()
	    var t1=dStart-now;
	    var t2=dEnd-now;
	    var strInfo="";
	    if(t1>0){
		strInfo=dhm(t1);
		info.statusValue=t1;
		if(t1<3600*1000){
		    info.status="upcomingSoon";
		}else{
		    info.status='upcoming';
		}
	    }else if(t1<=0 && t2>0){
		strInfo=dhm(t2)+" left";
		info.status='ongoing';
		info.statusValue=t2/(dEnd-dStart);
	    }else if(t2<0){
		strInfo="finished";
		info.status='finished';
	    }else{
		return info;
	    }
	    if(strInfo!="finished"){
		info.fn=fn.replace('[', '['+strInfo+'][');
	    }

	    var regexR=/^[^\[]+/ //to match the room name
	    t=fn.match(regexR);
	    if(!(t && t.length)){
		return info;
	    }
	    info.room=t[0].trim();
	    var regexBO=/\[\s*B[\s\w]*#\s*[\d\,]+\s*]/; //to match the square bracket for bo part
	    t=fn.match(regexBO);
	    if(!(t && t.length)){
		return info;
	    }
	    var regexBON=/[^\d]*([\d\,]+)[^\d]*/;
	    t=t[0].match(regexBON);
	    if(!(t && t.length>1)){
		return info;
	    }
	    info.bo=t[1].replace(/[\s\,]/, '')-0;
	    
	    return info;
	    
	}

	

	function dhm(t){
	    var cd = 24 * 60 * 60 * 1000,
		ch = 60 * 60 * 1000,
		d = Math.floor(t / cd),
		h = Math.floor( (t - d * cd) / ch),
		m = Math.round( (t - d * cd - h * ch) / 60000),
		pad = function(n){ return n < 10 ? '0' + n : n; };
	    if( m === 60 ){
		h++;
		m = 0;
	    }
	    if( h === 24 ){
		d++;
		h = 0;
	    }
	    h=pad(h);
	    m=pad(m);
	    
	    return (d?d+'days ':"")+(h?h+'hours ':'')+(m?m+'mins':'');
	}



	this.showfile=function(dirname, fn){
	    var inf=viewer(this.date, dirname, fn);
	    this.pdfUrl=inf.pdf?sce.trustAsResourceUrl(inf.pdf):null;
	    this.imgUrl=inf.img;
	    this.showIframe=!!this.pdfUrl;
	    this.showImg=!!this.imgUrl;
	    this.showFileList=!(this.pdfUrl||this.imgUrl);
	    console.info("url: ", this.pdfUrl||this.imgUrl);

	};
	this.returnToFileList=function(){
	    this.showFileList=true;
	    this.showIframe=false;
	    this.showImg=false;
	    this.pdfUrl=null;
	    this.imgUrl=null;
	};
	

	if(this.cookie) this.setDate(this.date);
	this.auth=function(){
	    if(debug)console.log(this.token);
	    if(this.token!='westinmontreal2016'){
		this.message="Wrong token, please contact Maxime";
		return;
	    }
	    this.cookie="ddd";
	    this.setDate(this.date);
	};

    }]);
    
    m.directive('date', function(){
	return{
	    restrict: "E",
	    template: "<input ng-model='this.date' type='date'></input>"
	};
    });

    
})();
