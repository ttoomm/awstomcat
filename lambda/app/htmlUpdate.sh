#!/bin/bash
files="listDaily.html js/rest.js js/westin.js"
#files="listDaily.html"
#files="media/f3.png media/f8.png media/f9.png media/f11.png media/tower.png"
bkName=pub.westin

cd static

for f in $files; do
    cmd="aws s3 cp $f s3://$bkName/staticHtml/$f --acl public-read"
    $cmd
done;

cd ..

