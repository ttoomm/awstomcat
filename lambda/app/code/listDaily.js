#!/usr/bin/node
'use strict';
process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
const Evt=require('events');
const AWS = require('aws-sdk');
require('./dateTool');

const s3=new AWS.S3();
const BucketName = 'westin.scanner';
const repoDir='dropbox';

//to get the bo number
var getBon=function( s){
    var b=s.split('/');
    var c=b[b.length-1];
    var d=decodeURIComponent(c);
    var e=d.replace(/^.+[^\d]+(\d+\s*\,\s*\d+)[^\d]+.+$/ , '$1');
    var f=e.replace(/[^\d]/g, '')
    return f; 
}


var listDir=function(bkName, prefix, em, evtName){
    s3.listObjectsV2({
	Bucket: bkName,
	//    ContinuationToken: 'STRING_VALUE',
	//    Delimiter: '/',
	EncodingType: 'url',
	MaxKeys: 999,
	Prefix: prefix
	//    StartAfter: prefix+"LE"
    }, function(err, data){
	em.emit(evtName, err, data);
    });
};


var handler=function(evt, ctx){

    if(ctx && ctx['http-method']=='OPTION'){
	ctx.succeed({allow: ['https://s3.amazonaws.com'], status: 200});
    }
    
    evt=evt||{};
    var pathDate=evt.params? (evt.params.path? evt.params.path.date: null):null;
    var date=evt.date||pathDate||((new Date).ymd('ymd'));
    var bktName=evt["stage-variables"]?evt['stage-variables']['bkname']:null;
    bktName=bktName||BucketName;
    //console.info(date);
    var prefix="/"+date.substr(0,6)+"/"+date.substr(6,8);

    var myevt=new Evt();
    var evtName="evt"+date;
    myevt.on(evtName, function(e, data){
	var _data=data.Contents ||[];
	var r={data: _data, error: e, evt: evt, ctx: ctx};
	if(e){
	    console.error(e);
	    r.status=500;
	    if(ctx)ctx.fail(r);
	    return;
	}else{
	    var data=[];
	    nextf: for(var i in r.data){
		var f=r.data[i];
		var bon=getBon(f.Key);
		console.info(f.Key, bon);
		var d=new Date(f.LastModiefied);
		if(f.Key.endsWith('pdf'))for(var j in data){
		    var ff=data[j];
		    var bbon=getBon(ff.Key);
		    var dd=new Date(ff.LastModiefied);
		    if((f.Key==ff.Key) || (bon==bbon)){
			
			if (dd.getTime()<d.getTime()){
			    data[j]=f;
			}
			continue nextf;
		    }
		}
		data.push(f);
	    }
	    console.info("returning data: ")
	    console.dir(data);
	    if(ctx)ctx.succeed({status: 200, data: data});
	}

    });
    listDir(bktName, repoDir+prefix, myevt, evtName);
    
};


module.exports={
    handler: handler
}




if(require.main===module){
    var evt={
	params: {
	    path: {
		date: '20161007'
	    }
	}
    };
    handler(evt, null);
}





