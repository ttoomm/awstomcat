#!/usr/bin/node
process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
// Load the SDK and UUID

require('./dateTool');
var AWS = require('aws-sdk');
var dbox=require('./dropboxTool');

const Evt=require('events');
const fs=require('fs');

const bucketName = 'westin.scanner';
const repoDir='dropbox';

var s3 = new AWS.S3();

var pathFromDboxToS3=function(path){
    return repoDir+path;
};

var pathFromS3ToDbox=function(path){
    if(path.length<8) return null;
    return path.substr(7);
};

var s3meta=function(bkt, fkey, evt, evtName){
    var param={
	Bucket: bkt,
	Key: fkey
    };
    s3.headObject(param, callback);
};



var s3ListFiles=function(bkt, strDate, days, evt, evtName){

    var _strDate=strDate.substr(0,4)+"-"+strDate.substr(4,2)+"-"+strDate.substr(6,2);
    var date=new Date(_strDate);
    for(var i=0; i<days; i++){
	var da=new Date(date.getTime()+24000*3600*i);
	var s=da.ymd('ym')+"/"+da.ymd('d');
	var paras={
	    Bucket: bkt,
	    Prefix: repoDir+"/"+s
	};

	s3.listObjects(paras, function(err, d){
	    evt.emit(evtName, err, d);
	});
    }
};



var syncFromDboxToS3=function(strDate, days){
    var evt=new Evt;
    var evtName='syncFromDboxToS3'+((new Date()).getTime());
    console.info(evtName);
    evt.on(evtName, function(o){

	var key=pathFromDboxToS3(o.path);
	var pathDbox=o.path;
	var dateDbox=new Date(o.lastModified);
	var sDbox=o.size;
	var version=o.version;
	console.info(o);
	s3.headObject({Bucket: bucketName, Key:key}, function(e, d){
	    if(e) {
		console.info("no exist in S3: "+key);
	    }else{
		var dateS3=new Date(d.LastModified);

		var sS3=d.ContentLength;
		
	    }
	    if(e||dateS3.getTime()<dateDbox.getTime()|| sS3!=sDbox){
		if(!e){
		    console.info({s3: dateS3, dbox: dateDbox});
		}
		console.info("downloading "+pathDbox);

		dbox.getFile(o.path, function(data){
		    var size=data && data.headers && (data.headers['content-length']? data.headers['content-length']: 0);
		    if(size!=sDbox){
			console.error("downloading error, the content length is: "+size+", the file size in dropbox is "+sDbox+"\n Detail:");
			console.error(data.headers);
			return;
		    }
		    //var body = fs.createReadStream(data);
		    var body=data;
		    //var s3obj=new AWS.S3({params: {Bucket: bucketName, key: key}});
		    var s3obj=new AWS.S3({params: {Bucket: bucketName}});
		    var upload=s3obj.upload({
			Body: body,
			ACL: 'public-read',
			Metadata:{
			    dbx_v:version
			},
			Key: key
		    });
		    upload.on('httpUploadProgress', function(_evt) {
			console.log(_evt);
		    });
		    upload.send(function(err, data) {
			if(err){
			    console.error("fail to download file, key:  "+key+"\n error: ");
			    console.error(err);
			    return;
			}
			console.info("downloaded file, key: "+key);
			console.log(data)
		    });
		    
		})
	    }

	});
	
    });
    dbox.listFiles(strDate, days, evt, evtName);
};

var rmNonExists=function(strDate, days){
    var evt=new Evt;
    var evtName='rmNonExist'+((new Date()).getTime());
    var evtNameDbox='listFile'+((new Date()).getTime());
    
    evt.on(evtName, function(e, d){
	if(e){
	    console.error(e);
	    return;
	}
	if(!d||!d.Contents || !(d.Contents instanceof Array)){
	    console.error("the data doesn't exist or the content is not array");
	    console.error(d);
	    return;
	}
	var es=d.Contents;
	es.forEach(function(e){
	    pathS3=e.Key;
	    var pathDbox=pathFromS3ToDbox(pathS3);
	    //console.info("s3: ", pathS3, " dbox: ", pathDbox);	
	    dbox.listFile(pathDbox, evt, evtNameDbox, {pathS3: pathS3});
	})
	
    });
    evt.on(evtNameDbox, function(e, d){
	if(e){
	    console.error(e);
	    //todo
	    //remove the file
	    return;
	}
	console.info(d);

    });
    
    s3ListFiles(bucketName, strDate, days, evt, evtName)
}


var sync=function(date, days){
    syncFromDboxToS3(date, days);
};




if(require.main===module){
    var evt=new Evt;
    var evtName='aa';
    evt.on(evtName, function(err, data){
	console.dir(data);
    });
    /*
    //    listDaily(bucketName, repoDir+"/201608/10", evt, evtName);
    */

    //syncFromDboxToS3(date, days);
    //    console.dir(dbox);
    //    rmNonExists(date, days);
    
    
    //    s3ListFiles(bucketName, "20160810", 40, evt, evtName);
    
    sync('20160826', 7);
};


handler=function(evt, ctx){
    console.info({evt: evt, ctx: ctx});
    evt=evt||{};
    evt.date=evt.date||((new Date()).ymd('ymd'));
    evt.days=evt.days||7;
    console.info(evt.date, evt.days);
    sync(evt.date, evt.days);
    
};

module.exports={
    handler: handler
}


