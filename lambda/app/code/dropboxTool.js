#!/usr/bin/node
var Dropbox = require('dropbox');
var token="IfZbjSsH1uAAAAAAAAAAzvC5p4z1_v817iG6y-DeDX-RP7bHUHeC3pssOZ5FwBwR";
var dbx = new Dropbox({ accessToken: token });

const Evt=require('events');
require('./dateTool');

var isDebug=false;

var listFiles=function(strDate, days, evt, evtName){
    var files=[];
    var _strDate=strDate.substr(0,4)+"-"+strDate.substr(4,2)+"-"+strDate.substr(6,2);
    var date=new Date(_strDate);
    for(var i=0; i<days; i++){
	var da=new Date(date.getTime()+i*24000*3600);
	var str="/"+da.ymd('ym')+'/'+da.ymd('d');
	if(isDebug){
	    console.info(str);
	}
	dbx.filesListFolder({path: str}).then(function(resp){
	    if(!resp ||! resp.entries)
		return;
	    var es=resp.entries;
	    es.forEach(function(e){
		if(!(e['.tag']=='folder')){
		    return;
		}
		var path= e.path_display;
		console.info(path);
		dbx.filesListFolder({path:path}).then(function(resp){
		    if(!resp || !resp.entries){
			return;
		    }
		    var es=resp.entries;
		    es.forEach(function(e){
			if(e['.tag']=='file'){
			    evt.emit(evtName, {
				path: e.path_display,
				size: e.size,
				lastModified: e.server_modified,
				version: e.rev
//				original: e
			    });
			}
		    });
		});

	    });
	});
    }
};

var listFile=function(path, evt, evtName, o){
    /*
    dbx.filesListFolder({path: path}).then(function(resp){
	evt.emit(evtName, resp);
    });
*/
    dbx.filesGetMetadata({path: path}).then(function(resp){
	o=o||{};
	o.resp=resp;
	evt.emit(evtName, o);
    });
};
const https=require('https');
const fs=require('fs');
//curl -X POST https://content.dropboxapi.com/2/files/download \
//    --header "Authorization: Bearer <get access token>" \
//    --header "Dropbox-API-Arg: {\"path\": \"/Homework/math/Prime_Numbers.txt\"}"
var getFile=function(path, callback){
    var opts={
	"Dropbox-API-Arg": "{\"path\": \""+ path+"\"}"
    };
    opts={
	host: "content.dropboxapi.com",
	path: '/2/files/download',
	method: "post",
	headers: {
	    Authorization: "Bearer "+ token,
	    "Dropbox-API-Arg": '{\"path\": "'+path+'"}'
	}
    }
    

    https.request(opts, function(resp){
	callback(resp);
    }).end();
    
};

module.exports={
    listFiles: listFiles,
    listFile: listFile,
    getFile: getFile
};

var sampleFile='/201608/10/Bos/autogen_layout.png';

if(require.main===module){
    var evt=new Evt;
    evt.on('fff', function(e){
	console.dir(e);
    });
    listFiles('20160804',30, evt, 'fff');
    /*
    //listFile(sampleFile, evt, 'fff');
    getFile(sampleFile, function(d){
	console.dir(d.headers);

//	console.dir(d.headers['content-length']);
	d.pipe(fs.createWriteStream('./a.png'));
    });
*/
}




