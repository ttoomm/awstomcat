#!/usr/bin/node

var Dropbox = require("./dropbox");
var fs=require("fs");
var streamifier=require("./streamifier");


var auth={
    key:"5f6nxxy1onysb1a",
    secret: 'zww3ioql5iiud5m',
    token: 'IfZbjSsH1uAAAAAAAAAACw6y6fJuOKu8Qx-orkBr8OY7JFgMj2JwD3yo9roKiLV-'
};

var client = new Dropbox.Client(auth);

Date.prototype.ymd = function(e) {
  var yyyy = this.getFullYear().toString();
  var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
  var dd  = this.getDate().toString();
  if(e=='ymd'){
    return yyyy + (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0]); // padding
  }else if(e=='ym'){
    return yyyy + (mm[1]?mm:"0"+mm[0]);
  }else if(e=='d'){
    return (dd[1]?dd:"0"+dd[0]);
  }
};




var syncFileTo=function(client, remote, localstream, callback){

  client.readFile(remote, {buffer: true}, function(err, data, meta, rangeInfo){
    if(err){
      if(callback){
	callback(err, null);
      }else{
	console.dir(err);
      }
      return;
    }
    console.dir(rangeInfo);
    var reader=streamifier.createReadStream(data);
    reader.pipe(localstream);
    reader.on('end', function(){
      console.log("end");
      callback(null, meta);
    });
  });

};

var syncFileFrom=function(client, remote,callback){
  if(!callback) return;
  
  client.readFile(remote, {buffer: true}, function(err, data, meta, rangeInfo){
    if(err){
      if(callback){
	callback(err, null);
      }else{
	console.dir(err);
      }
      return;
    }
    console.dir(rangeInfo);
    var reader=streamifier.createReadStream(data);
    callback(null, reader);
    });
};

var syncFileContent=function(client, remote,callback){
  if(!callback) return;
  
  client.readFile(remote, {buffer: true}, function(err, data, meta, rangeInfo){
    if(err){
      if(callback){
	callback(err, null);
      }else{
	console.dir(err);
      }
      return;
    }
    console.dir(rangeInfo);
    callback(null, data);
    });
};

/*
 get the entries in /yyyymm/dd/xxxx/xxxx.pdf
 */

var cb=function(e, d){
  if(e){
    console.error("error: ");
    console.dir(e);
  }else{
    console.log("data: ");
    console.log(d);
  }
};

var getDay=function(client, path, callback){
  var listFile=function(client, dirs, callback, result){
    var dir=dirs.pop();
    var eleLeft=dirs.length;
    client.readdir(dir, function(err, es, st, sts){
      if(err){
	callback(err, null);
	return;
      }
      var _es=sts.map(function(e){
	if(!e.isFile){
	  return null;
	}
	return {
	  path: e.path,
	  version: e.versionTag,
	  size: e.size
	};
      });
      
      result=result.concat(_es);
      if(eleLeft>0){
	listFile(client, dirs, callback, result);
      }
      else callback(null, result);
    });
  };

  client.readdir(path, function(err, es, st, sts){
    if(err){
      callback(err, null);
      return;
    }
    var _dirs=sts.map(function(e){
      if(!e.isFolder) return null;
      return e.path;
    });
    var r=[];
    listFile(client, _dirs, callback, r);
  });

};


exports.dailyInfo=function(day, callback){
  var ymd="/"+day.ymd('ym')+"/"+day.ymd('d');
  //var ymd='/201603/17';
  client.authenticate({interactive: false}, function(e, client){
    if(e){
      callback(e, null);
      return;
    }
    //console.log(ymd);
    getDay(client, ymd, callback?callback: cb);
  });
};
exports.saveFile=function(remote, localStream, callback){
  client.authenticate(function(err, client){
    if(err){
      callback(err, null);
      return;
    }
    syncFileTo(client, remote, localStream, callback?callback:cb);
  });
};

exports.getFileContent=function(remote, callback){
  if(!callback) return;
  client.authenticate(function(err, client){
    if(err){
      callback(err, null);
      return;
    }
    syncFileContent(client, remote, callback);
  });
};



/*
var day=new Date((new Date())-1000*24*3600*2);
exports.dailyInfo(day, function(e, d){
  console.log(d);
});


var testfile='/201603/27/Bos/ 30,806 Tuesday, October 27, 2015.pdf';

var ls=fs.createWriteStream('/home/hh/tmp/testfile.pdf');

exports.getFile(testfile, ls, function(e,d){
  console.log(d);
});


return;
*/










