#!/usr/bin/node
var Dropbox = require('dropbox');
var token="IfZbjSsH1uAAAAAAAAAAzvC5p4z1_v817iG6y-DeDX-RP7bHUHeC3pssOZ5FwBwR";
var dbx = new Dropbox({ accessToken: token });

const Evt=require('events');
require('./dateTool');

var isDebug=false;


var listFiles=function(strDate, days, evt, evtName){
    var files=[];
    var _strDate=strDate.substr(0,4)+"-"+strDate.substr(4,2)+"-"+strDate.substr(6,2);
    var date=new Date(_strDate);
    for(var i=0; i<days; i++){
	var da=new Date(date.getTime()+i*24000*3600);
	var str="/"+da.ymd('ym')+'/'+da.ymd('d');
	if(isDebug){
	    console.info(str);
	}
	dbx.filesListFolder({path: str}).then(function(resp){
	    if(!resp ||! resp.entries)
		return;
	    var es=resp.entries;
	    es.forEach(function(e){
		if(!(e['.tag']=='folder')){
		    return;
		}
		var path= e.path_display;
		dbx.filesListFolder({path:path}).then(function(resp){
		    if(!resp || !resp.entries){
			return;
		    }
		    var es=resp.entries;
		    es.forEach(function(e){
			if(e['.tag']=='file'){
			    evt.emit(evtName, {
				path: e.path_display,
				rev: e.rev,
				fs: e.size,
				modifiedAt: e.server_modified,
				original: e
			    });
			}
		    });
		});

	    });
	});
    }
};


exports.listFiles=listFiles;

if(require.main===module){
    var evt=new Evt;
    evt.on('fff', function(e){console.dir(e.path);});
    getFiles('20160804',8, evt, 'fff');
}




